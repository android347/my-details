package com.example.mydetails

import android.os.Bundle
import android.util.Patterns
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.textfield.TextInputLayout
import com.google.firebase.firestore.FirebaseFirestore

class MainActivity : AppCompatActivity() {

    private lateinit var submitBtm: Button
    private lateinit var name: TextInputLayout
    private lateinit var email: TextInputLayout
    private lateinit var username: TextInputLayout
    private lateinit var password: TextInputLayout
    private lateinit var confirmPassword: TextInputLayout


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        submitBtm = findViewById(R.id.button)
        name = findViewById(R.id.nameField)
        email = findViewById(R.id.emailField)
        username = findViewById(R.id.userField)
        password = findViewById(R.id.passwordField)
        confirmPassword = findViewById(R.id.confirmPasswordField)




        submitBtm.setOnClickListener {

            if (validate()) {
                val db = FirebaseFirestore.getInstance()
                val item = mapOf(
                    "name" to name.editText?.text.toString(),
                    "email" to email.editText?.text.toString(),
                    "username" to username.editText?.text.toString(),
                    "password" to password.editText?.text.toString(),
                    "confirmPassword" to confirmPassword.editText?.text.toString(),
                )

                db.collection("test")
                    .add(item)
                    .addOnSuccessListener {
                        Toast.makeText(applicationContext, "Success", Toast.LENGTH_SHORT).show()
                    }
                    .addOnFailureListener {
                        Toast.makeText(applicationContext, "Failure", Toast.LENGTH_SHORT).show()
                    }
            } else {
                Toast.makeText(applicationContext, "Invalid Data", Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun validate(): Boolean {
        if (name.editText != null) {
            if (name.editText!!.text != null && name.editText!!.text.isEmpty()) {
                name.editText!!.error = "Please Enter Value"
                return false
            }
        }
        if (email.editText != null) {
            if (email.editText!!.text != null && email.editText!!.text.isEmpty()) {
                email.editText!!.error = "Please Enter Value"
                return false

            }
            fun CharSequence?.isValidEmail() =
                !isNullOrEmpty() && Patterns.EMAIL_ADDRESS.matcher(this).matches()

            if (!email.editText!!.text.isValidEmail()) {
                email.editText!!.error = "Please Enter Valid Value"
                return false
            }
        }
        if (username.editText != null) {
            if (username.editText!!.text != null && username.editText!!.text.isEmpty()) {
                username.editText!!.error = "Please Enter Value"
                return false
            }
        }
        if (password.editText != null) {
            if (password.editText!!.text != null && password.editText!!.text.isEmpty()) {
                password.editText!!.error = "Please Enter Value"
                return false
            }

            if (!isValidPassword(password.editText!!.text.toString())) {
                password.editText!!.error = "Please Enter Valid Value"
                return false
            }
        }
        if (confirmPassword.editText != null) {
            if (confirmPassword.editText!!.text != null && confirmPassword.editText!!.text.isEmpty()) {
                confirmPassword.editText!!.error = "Please Enter Value"
                return false
            }

            if (!isValidPassword(confirmPassword.editText!!.text.toString())) {
                confirmPassword.editText!!.error = "Please Enter Valid Value"
                return false
            }

            if (confirmPassword.editText!!.text.toString() != password.editText!!.text.toString()) {
                confirmPassword.editText!!.error = "Password Doesn't match"
                return false
            }
        }
        return true
    }

    private fun isValidPassword(password: String): Boolean {
        if (password.length < 8) return false
        if (password.filter { it.isDigit() }.firstOrNull() == null) return false
        if (password.filter { it.isLetter() }.filter { it.isUpperCase() }
                .firstOrNull() == null) return false
        if (password.filter { it.isLetter() }.filter { it.isLowerCase() }
                .firstOrNull() == null) return false
        if (password.filter { !it.isLetterOrDigit() }.firstOrNull() == null) return false

        return true
    }
}